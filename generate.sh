#!/bin/sh

if [ "$#" -ne 1 ]; then
    echo "Please call the script with the build type as argument. Example: ./generate.sh Debug"
    exit 1
fi

mkdir Build

conan config install ./Config/conan/user

conan install . --profile clang -pr:b=default --build=missing --no-imports -if Build -s build_type="$1"

conan imports . -if Build/ -imf .

cmake -S . -B Build -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_TOOLCHAIN_FILE=Build/conan_toolchain.cmake -DCMAKE_BUILD_TYPE="$1"