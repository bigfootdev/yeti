cmake_minimum_required(VERSION 3.19)

project(Yeti VERSION 0.1.0
                DESCRIPTION "The Yeti editor"
                LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)

set(CMAKE_CONFIGURATION_TYPES "Release;RelWithDebInfo;Debug" CACHE STRING "" FORCE)

option(VULKAN OFF)
option(NO_TRACY OFF)
option(UNITY_BUILD ON)

include(${CMAKE_SOURCE_DIR}/CMake/Common.cmake)
include(${CMAKE_SOURCE_DIR}/CMake/Application.cmake)

if(WIN32)
    set(PLATFORM_COMPILE_DEFINITION BIGFOOT_WINDOWS)
elseif(UNIX AND NOT APPLE)
    set(PLATFORM_COMPILE_DEFINITION BIGFOOT_LINUX)
endif()

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

find_package(bigfoot REQUIRED)
if(NOT TARGET bigfoot::BigfootShaderExporterExecutable)
    add_executable(bigfoot::BigfootShaderExporterExecutable IMPORTED GLOBAL)
    set_target_properties(bigfoot::BigfootShaderExporterExecutable PROPERTIES IMPORTED_LOCATION "${bigfoot_INCLUDE_DIR}/../bin/tools/BigfootShaderExporterExecutable")
endif()
if(NOT TARGET bigfoot::BigfootModelExporterExecutable)
    add_executable(bigfoot::BigfootModelExporterExecutable IMPORTED GLOBAL)
    set_target_properties(bigfoot::BigfootModelExporterExecutable PROPERTIES IMPORTED_LOCATION "${bigfoot_INCLUDE_DIR}/../bin/tools/BigfootModelExporterExecutable")
endif()
if(NOT TARGET bigfoot::BigfootCommonExporterExecutable)
    add_executable(bigfoot::BigfootCommonExporterExecutable IMPORTED GLOBAL)
    set_target_properties(bigfoot::BigfootCommonExporterExecutable PROPERTIES IMPORTED_LOCATION "${bigfoot_INCLUDE_DIR}/../bin/tools/BigfootCommonExporterExecutable")
endif()

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Yeti)
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT Yeti)