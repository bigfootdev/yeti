function(add_resource_folder ApplicationName ApplicationRes ParentFolder)
    file(GLOB_RECURSE SOURCES CONFIGURE_DEPENDS ${ApplicationRes}/*)
    get_filename_component(RES_FOLDER_NAME ${ApplicationRes} NAME)

    set(OUTPUT_PATH_BIGFILE "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${RES_FOLDER_NAME}/${ApplicationName}-bigfile.db")
    get_filename_component(OUTPUT_PATH_BIGFILE_ABSOLUTE ${OUTPUT_PATH_BIGFILE} ABSOLUTE)
    get_filename_component(OUTPUT_PATH_BIGFILE_DIRECTORY ${OUTPUT_PATH_BIGFILE_ABSOLUTE} DIRECTORY)
    get_filename_component(APPLICATION_RES_ABSOLUTE ${ApplicationRes} ABSOLUTE)

    add_custom_command(
        OUTPUT ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp ${OUTPUT_PATH_BIGFILE_ABSOLUTE}
        DEPENDS ${bigfoot_INCLUDE_DIR}/../tools/SQL/BigFileSchema.sql
        COMMAND ${CMAKE_COMMAND} -E make_directory ${OUTPUT_PATH_BIGFILE_DIRECTORY}
        COMMAND sqlite3 ${OUTPUT_PATH_BIGFILE_ABSOLUTE} < ${bigfoot_INCLUDE_DIR}/../tools/SQL/BigFileSchema.sql
        COMMAND ${CMAKE_COMMAND} -E touch ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp
        COMMENT "Creating Bigfile ${OUTPUT_PATH_BIGFILE_ABSOLUTE}"
    )
    list(APPEND RES_SOURCES ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp)

    foreach(SOURCE_FILE ${SOURCES})
        file(RELATIVE_PATH SOURCE_FILE_REL_PATH ${ApplicationRes} ${SOURCE_FILE}.bftimestamp)

        set(OUTPUT_PATH "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${RES_FOLDER_NAME}/${SOURCE_FILE_REL_PATH}")
        get_filename_component(OUTPUT_PATH_ABSOLUTE ${OUTPUT_PATH} ABSOLUTE)
        get_filename_component(OUTPUT_PATH_DIRECTORY ${OUTPUT_PATH_ABSOLUTE} DIRECTORY)

        list(APPEND RES_SOURCES ${OUTPUT_PATH_ABSOLUTE})

        if(SOURCE_FILE MATCHES "\\.(glsl)$")
            get_filename_component(GLSL_FILENAME ${SOURCE_FILE} NAME)

            if (${GLSL_FILENAME} MATCHES "([^*/]+)\\.(vert|frag)\\.glsl$")
                set(SHADER_STAGE ${CMAKE_MATCH_2})

                if(${SHADER_STAGE} STREQUAL "vert")
                    set(SHADER_VALUE 0)
                elseif(${SHADER_STAGE} STREQUAL "frag")
                    set(SHADER_VALUE 1)
                endif()

                add_custom_command(
                    OUTPUT ${OUTPUT_PATH_ABSOLUTE}
                    DEPENDS ${SOURCE_FILE} ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp
                    COMMAND ${CMAKE_COMMAND} -E make_directory ${OUTPUT_PATH_DIRECTORY}
                    COMMAND bigfoot::BigfootShaderExporterExecutable -f ${SOURCE_FILE} -b ${APPLICATION_RES_ABSOLUTE} -B ${OUTPUT_PATH_BIGFILE_ABSOLUTE} -t ${SHADER_VALUE}
                    COMMAND ${CMAKE_COMMAND} -E touch ${OUTPUT_PATH_ABSOLUTE}
                    COMMENT "Exporting shader ${SOURCE_FILE} to ${OUTPUT_PATH_BIGFILE_ABSOLUTE}"
                )
            endif()
        elseif(SOURCE_FILE MATCHES "\\.(fbx)$")
            add_custom_command(
                OUTPUT ${OUTPUT_PATH_ABSOLUTE}
                DEPENDS ${SOURCE_FILE} ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp
                COMMAND ${CMAKE_COMMAND} -E make_directory ${OUTPUT_PATH_DIRECTORY}
                COMMAND bigfoot::BigfootModelExporterExecutable -f ${SOURCE_FILE} -b ${APPLICATION_RES_ABSOLUTE} -B ${OUTPUT_PATH_BIGFILE_ABSOLUTE}
                COMMAND ${CMAKE_COMMAND} -E touch ${OUTPUT_PATH_ABSOLUTE}
                COMMENT "Exporting FBX ${SOURCE_FILE} to ${OUTPUT_PATH_BIGFILE_ABSOLUTE}"
            )
        else()
            add_custom_command(
                OUTPUT ${OUTPUT_PATH_ABSOLUTE}
                DEPENDS ${SOURCE_FILE} ${OUTPUT_PATH_BIGFILE_ABSOLUTE}.bftimestamp
                COMMAND ${CMAKE_COMMAND} -E make_directory ${OUTPUT_PATH_DIRECTORY}
                COMMAND bigfoot::BigfootCommonExporterExecutable -f ${SOURCE_FILE} -b ${APPLICATION_RES_ABSOLUTE} -B ${OUTPUT_PATH_BIGFILE_ABSOLUTE}
                COMMAND ${CMAKE_COMMAND} -E touch ${OUTPUT_PATH_ABSOLUTE}
                COMMENT "Exporting ${SOURCE_FILE} to ${OUTPUT_PATH_BIGFILE_ABSOLUTE}"
            )
        endif()
    endforeach()

    add_custom_target(${ApplicationName}Resources ALL DEPENDS ${RES_SOURCES})
    set_target_properties(${ApplicationName}Resources PROPERTIES FOLDER ${ParentFolder})
    
    target_sources(${ApplicationName} PRIVATE ${SOURCES})
    add_dependencies(${ApplicationName} ${ApplicationName}Resources)

    source_group(TREE ${ApplicationRes} PREFIX Res/ FILES ${SOURCES})
endfunction()

function(copy_folder ApplicationName Folder ParentFolder)
    file(GLOB_RECURSE SOURCES CONFIGURE_DEPENDS ${Folder}/*)
    get_filename_component(FOLDER_NAME ${Folder} NAME)

    foreach(SOURCE_FILE ${SOURCES})
        file(RELATIVE_PATH SOURCE_FILE_REL_PATH ${Folder} ${SOURCE_FILE})

        set(OUTPUT_PATH "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${FOLDER_NAME}/${SOURCE_FILE_REL_PATH}")
        get_filename_component(OUTPUT_PATH_ABSOLUTE ${OUTPUT_PATH} ABSOLUTE)

        if(NOT (${SOURCE_FILE} STREQUAL ${OUTPUT_PATH_ABSOLUTE}))
            add_custom_command(
                OUTPUT ${OUTPUT_PATH_ABSOLUTE}
                COMMAND ${CMAKE_COMMAND} -E copy "${SOURCE_FILE}" "${OUTPUT_PATH_ABSOLUTE}"
                DEPENDS ${SOURCE_FILE}
                COMMENT "Copying ${SOURCE_FILE} to ${OUTPUT_PATH_ABSOLUTE}"
            )
        endif()

        list(APPEND RES_SOURCES ${OUTPUT_PATH_ABSOLUTE})
    endforeach()

    add_custom_target(${ApplicationName}${FOLDER_NAME} ALL DEPENDS ${RES_SOURCES})
    set_target_properties(${ApplicationName}${FOLDER_NAME} PROPERTIES FOLDER ${ParentFolder})

    target_sources(${ApplicationName} PRIVATE ${SOURCES})
    add_dependencies(${ApplicationName} ${ApplicationName}${FOLDER_NAME})

    source_group(TREE ${Folder} PREFIX ${FOLDER_NAME}/ FILES ${SOURCES})
endfunction()