function(create_application ApplicationName ApplicationSource ApplicationRes ApplicationPrivateDependencies ParentFolder)
    project(${ApplicationName})

    file(GLOB_RECURSE SRC CONFIGURE_DEPENDS ${ApplicationSource}/*.h ${ApplicationSource}/*.cpp)

    add_executable(${PROJECT_NAME} ${SRC})
    set_target_properties(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

    target_include_directories(${PROJECT_NAME} PUBLIC ${ApplicationSource})
    
    target_compile_definitions(${PROJECT_NAME}
                                PUBLIC NOMINMAX
                                PUBLIC WIN32_LEAN_AND_MEAN
                                PUBLIC $<$<CONFIG:Release>:BIGFOOT_RELEASE>
                                PUBLIC $<$<CONFIG:RelWithDebInfo>:BIGFOOT_RelWithDebInfo>
                                PUBLIC $<$<CONFIG:Debug>:BIGFOOT_DEBUG>
                                PUBLIC "${PLATFORM_COMPILE_DEFINITION}")
    if(NO_TRACY)
        target_compile_definitions(${PROJECT_NAME}
                                    PUBLIC $<$<BOOL:NO_TRACY>:NO_TRACY>)
    endif()

    if(VULKAN)
        target_compile_definitions(${PROJECT_NAME} PUBLIC BIGFOOT_VULKAN)
    endif()
    
    foreach(Dependency ${ApplicationPrivateDependencies})
        target_link_libraries(${PROJECT_NAME} PRIVATE ${Dependency})
    endforeach()
    
    target_link_libraries(${PROJECT_NAME} PRIVATE bigfoot::bigfoot)

    target_compile_options(${PROJECT_NAME} PRIVATE
        $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX>
        $<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-Wall -Wextra -Wpedantic -Werror>)
    
    if ((MSVC) AND (MSVC_VERSION GREATER_EQUAL 1914))
        target_compile_options(${PROJECT_NAME} PRIVATE "/Zc:__cplusplus")
        target_compile_options(${PROJECT_NAME} PRIVATE "/Zc:preprocessor")
    endif()
    
    source_group(TREE ${ApplicationSource} PREFIX Src FILES ${SRC})
    
    set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER ${ParentFolder})

    add_resource_folder(${ApplicationName} ${ApplicationRes} ${ParentFolder})
endfunction()