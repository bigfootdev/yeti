if not exist "Build" mkdir Build

conan config install ./Config/conan/user

conan install . --profile msvc -pr:b=default --build=missing --no-imports -if Build -s build_type=Release
conan install . --profile msvc -pr:b=default --build=missing --no-imports -if Build -s build_type=RelWithDebInfo
conan install . --profile msvc -pr:b=default --build=missing --no-imports -if Build -s build_type=Debug

conan imports . -if Build/ -imf .

cmake -S . -B Build -DCMAKE_TOOLCHAIN_FILE=Build/conan_toolchain.cmake
