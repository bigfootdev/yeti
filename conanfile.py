from conans import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake

required_conan_version = ">=1.33.0"

class Bigfoot(ConanFile):
    name = "yeti"
    homepage = "https://gitlab.com/bigfootdev/yeti"
    description = "Yeti is an editor using Bigfoot"
    topics = ("game engine", "3d")
    license = "MIT"
    version = "0.1.0"

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "vulkan": [True, False],
        "no_tracy": [True, False],
        "unity_build": [True, False]
        }
    default_options = {
        "shared": False,
        "fPIC": True,
        "vulkan": True,
        "no_tracy": False,
        "unity_build": True
        }
    
    generators = "CMakeDeps"

    def configure(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

        self.options["bigfoot"].unity_build = self.options.unity_build
        self.options["bigfoot"].build_tests = False
        self.options["bigfoot"].disable_rendering_tests = True
        self.options["bigfoot"].sample_app = False
        self.options["bigfoot"].vulkan = self.options.vulkan
        self.options["bigfoot"].no_tracy = self.options.no_tracy
        self.options["bigfoot"].build_tools = True

    def requirements(self):
        self.requires("bigfoot/0.4.0@bigfoot/main")
        self.requires("sqlite3/3.41.2")
        self.requires("qt/6.2.4")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["UNITY_BUILD"] = self.options.unity_build
        tc.variables["VULKAN"] = self.options.vulkan
        tc.variables["NO_TRACY"] = self.options.no_tracy
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()