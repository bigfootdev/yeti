/*****************************************************************//**
 * \file   GameLayer.h
 * 
 * \author Romain BOULLARD
 * \date   April 2023
 *********************************************************************/
#pragma once
#include <Engine/Layer/Layer.h>

#include <Graphics/Core/Mesh/Mesh.h>
#include <Graphics/Core/Model/Model.h>

#include <Graphics/Interface/Program/Program.h>
#include <Graphics/Interface/Shader/Shader.h>

namespace Yeti
{
	class GameLayer final : public Bigfoot::Layer
	{
	public:
		GameLayer();

		GameLayer(const GameLayer& p_layer) = delete;
		GameLayer(GameLayer&& p_layer) = delete;

		~GameLayer() override;

		/**
		 * Called when the layer is attached to the manager.
		 *
		 * \param p_rendererAPI The renderer
		 */
		void OnAttach(Bigfoot::Graphics::RendererAPI& p_rendererAPI) override;

		/**
		 * Called when the layer is detached from the manager.
		 *
		 */
		void OnDetach() override;

		/**
		 * Called each frame.
		 *
		 */
		void OnUpdate() override;

		/**
		 * Called each frame.
		 *
		 * \param p_rendererAPI The renderer
		 */
		void OnUpdateGraphic(Bigfoot::Graphics::RendererAPI& p_rendererAPI) override;

		GameLayer& operator=(const GameLayer& p_layer) = delete;
		GameLayer& operator=(GameLayer&& p_layer) = delete;
	private:
		Bigfoot::Graphics::ModelLibrary m_modelLibrary;
		Bigfoot::Graphics::MeshLibrary m_meshLibrary;
		Bigfoot::Graphics::ShaderLibrary m_shaderLibrary;
		Bigfoot::Graphics::ProgramLibrary m_programLibrary;

		Bigfoot::Graphics::ModelLibrary::Handle m_model;

		Bigfoot::Graphics::ShaderLibrary::Handle m_vertexShader;
		Bigfoot::Graphics::ShaderLibrary::Handle m_fragmentShader;

		Bigfoot::Graphics::ProgramLibrary::Handle m_program;
	};
}
