/*****************************************************************//**
 * \file   GameLayer.cpp
 * 
 * \author Romain BOULLARD
 * \date   April 2023
 *********************************************************************/
#include <Layer/GameLayer.h>

#include <Graphics/Core/RendererAPI/RendererAPI.h>

#include <System/BigFile/BigFile.h>
#include <System/BigFile/Data/BigFileData.h>
#include <System/BigFile/Entry/BigFileEntry.h>

namespace Yeti
{
	GameLayer::GameLayer() :
		Bigfoot::Layer("Gamelayer")
	{
	}

	/****************************************************************************************/

	GameLayer::~GameLayer()
	{

	}

	/****************************************************************************************/

	void GameLayer::OnAttach(Bigfoot::Graphics::RendererAPI& p_rendererAPI)
	{
		const Bigfoot::BigFileEntry vertexShader{ Bigfoot::BigFileSingleton::Instance().GetEntry("Shader/Mesh/mesh.vert.glsl") };
		const Bigfoot::BigFileEntry fragmentShader{ Bigfoot::BigFileSingleton::Instance().GetEntry("Shader/Mesh/mesh.frag.glsl") };

		const Bigfoot::FastVector<Bigfoot::Graphics::Vertex> vertices{std::initializer_list<Bigfoot::Graphics::Vertex>{
			{{0.0f, -0.5f, 0.0f}},
			{{0.5f, 0.5f, 0.0f}},
			{{-0.5f, 0.5f, 0.0f}}
		}};
		const Bigfoot::FastVector<std::uint32_t> indices{std::initializer_list<std::uint32_t>{
			0, 1, 2
		}};

		const Bigfoot::Graphics::MeshLibrary::Handle mesh = m_meshLibrary.AddResource(Bigfoot::MakeShared<Bigfoot::Graphics::Mesh>(p_rendererAPI, vertices, indices, "Mesh"));

		m_vertexShader = m_shaderLibrary.AddResource(p_rendererAPI.CreateShader(Bigfoot::Graphics::ShaderType::ShaderType_Vertex, vertexShader.GetData("SPV").Get<Bigfoot::FastVector<std::byte>>(), "VertexShader"));
		m_fragmentShader = m_shaderLibrary.AddResource(p_rendererAPI.CreateShader(Bigfoot::Graphics::ShaderType::ShaderType_Fragment, fragmentShader.GetData("SPV").Get<Bigfoot::FastVector<std::byte>>(), "FragmentShader"));

		m_program = m_programLibrary.AddResource(p_rendererAPI.CreateProgram(p_rendererAPI.GetRenderPass(), { m_vertexShader, m_fragmentShader }, p_rendererAPI.Size(), "Program"));

		m_model = m_modelLibrary.AddResource(Bigfoot::MakeShared<Bigfoot::Graphics::Model>(mesh, m_program, "Model"));
	}

	/****************************************************************************************/

	void GameLayer::OnDetach()
	{

	}

	/****************************************************************************************/

	void GameLayer::OnUpdate()
	{

	}

	/****************************************************************************************/

	void GameLayer::OnUpdateGraphic(Bigfoot::Graphics::RendererAPI& p_rendererAPI)
	{
		p_rendererAPI.Draw(*m_model.GetResource().lock());
	}
}
