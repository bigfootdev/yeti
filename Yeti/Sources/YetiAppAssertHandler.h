/*****************************************************************//**
 * \file   YetiAssertHandler.h
 * 
 * \author Romain BOULLARD
 * \date   February 2023
 *********************************************************************/
#pragma once
#if defined(BIGFOOT_DEBUG) || defined(BIGFOOT_RELWITHDEBINFO)
#include <Utils/Assert/Assert.h>

#include <System/Logger/Logger.h>

namespace Yeti
{
	class YetiAssertHandler
	{
	public:
		YetiAssertHandler() = delete;

		YetiAssertHandler(const YetiAssertHandler& p_handler) = delete;
		YetiAssertHandler(YetiAssertHandler&& p_handler) = delete;

		~YetiAssertHandler() = delete;

		/**
		* Handle an assertion.
		*
		* \param p_location Location of the assertion.
		* \param p_format Format string for the assertion message.
		* \param ...p_args Arguments for the format string.
		*/
		template<typename... ARGS>
		static void Handle(const Bigfoot::AssertLocation& p_location, fmt::format_string<ARGS...>&& p_format, ARGS&&... p_args)
		{
			const Bigfoot::String assertMessage = fmt::format(p_format, std::forward<ARGS>(p_args)...).c_str();

			LOG_FATAL(APPLICATION_LOGGER, "Assert: {} (File:{}, Line:{}, Function:{}", assertMessage, p_location.m_fileName, p_location.m_line, p_location.m_functionName)
		}

		YetiAssertHandler& operator=(const YetiAssertHandler& p_handler) = delete;
		YetiAssertHandler& operator=(YetiAssertHandler&& p_handler) = delete;
	};
}
#endif