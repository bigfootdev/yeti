/*****************************************************************//**
 * \file   YetiApplication.cpp
 * 
 * \author Romain BOULLARD
 * \date   December 2022
 *********************************************************************/
#include <Application/YetiApplication.h>

#include <Application.config.h>

#include <Engine/Engine.config.h>
#include <Engine/EntryPoint.h>

#include <Layer/GameLayer.h>

#include <Window/HeadlessWindow.h>

#include <qapplication.h>

namespace Yeti
{
	[[nodiscard]] Bigfoot::Graphics::GraphicContextCreateInfo ComputeGraphicContextCreateInfo(const Bigfoot::Graphics::BigfootWindow& p_window)
	{
		return Bigfoot::Graphics::GraphicContextCreateInfo{
			Bigfoot::BIGFOOT_VERSION,
			Bigfoot::BIGFOOT_NAME,
			YETI_VERSION,
			YETI_NAME,
			p_window.FramebufferSize(),
#ifdef BIGFOOT_VULKAN
			Bigfoot::Graphics::GraphicContextCreateInfo::Vulkan{
				p_window.RequiredExtensions(),
				{},
				{},
				{},
#if defined BIGFOOT_WINDOWS
				p_window.GetInstance(),
				p_window.GetWindow()
#elif defined BIGFOOT_LINUX
				p_window.GetX11Display(),
				p_window.GetX11Window(),
				p_window.GetWaylandDisplay(),
				p_window.GetWaylandSurface()
#endif
			}
#endif
		};
	}

	/****************************************************************************************/

	YetiApplication::YetiApplication(const Bigfoot::Graphics::GraphicContextCreateInfo& p_graphicContextCreateInfo, Bigfoot::UniquePtr<Bigfoot::Graphics::BigfootWindow> p_window) :
		Bigfoot::Application(p_graphicContextCreateInfo, std::move(p_window))
	{
		LOG_INFO(APPLICATION_LOGGER, "Application created!");

		AddLayer(Bigfoot::MakeUnique<GameLayer>());
	}
}

/****************************************************************************************/

Bigfoot::UniquePtr<Bigfoot::Application> Bigfoot::CreateApplication()
{
	Bigfoot::UniquePtr<Bigfoot::Graphics::BigfootWindow> window = Bigfoot::MakeUnique<Yeti::HeadlessWindow>();
	const Bigfoot::Graphics::GraphicContextCreateInfo graphicContextCreateInfo = Yeti::ComputeGraphicContextCreateInfo(*window);

	return Bigfoot::MakeUnique<Yeti::YetiApplication>(graphicContextCreateInfo, Bigfoot::Move(window));
}

/****************************************************************************************/

Bigfoot::File Bigfoot::BigFileLocation()
{
	return Yeti::YETI_BIGFILE;
}
