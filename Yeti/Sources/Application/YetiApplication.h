/*****************************************************************//**
 * \file   YetiApplication.h
 * 
 * \author Romain BOULLARD
 * \date   December 2022
 *********************************************************************/
#pragma once
#include <Engine/Application/Application.h>

namespace Yeti
{
	class YetiApplication final : public Bigfoot::Application
	{
	public:
		/**
		 * Constructor.
		 * 
		 */
		YetiApplication(const Bigfoot::Graphics::GraphicContextCreateInfo& p_graphicContextCreateInfo, Bigfoot::UniquePtr<Bigfoot::Graphics::BigfootWindow> p_window);

		YetiApplication(const YetiApplication& p_application) = delete;
		YetiApplication(YetiApplication&& p_application) = delete;

		~YetiApplication() override = default;

		YetiApplication& operator=(const YetiApplication& p_application) = delete;
		YetiApplication& operator=(YetiApplication&& p_application) = delete;
	};
}
