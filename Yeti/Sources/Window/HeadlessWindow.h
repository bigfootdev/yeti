/*****************************************************************//**
 * \file   HeadlessWindow.h
 * 
 * \author Romain BOULLARD
 * \date   April 2023
 *********************************************************************/
#pragma once
#include <Graphics/Core/Window/Window.h>

namespace Yeti
{
	class HeadlessWindow final : public Bigfoot::Graphics::BigfootWindow
	{
	public:
		HeadlessWindow() = default;

		HeadlessWindow(const HeadlessWindow& p_window) = delete;
		HeadlessWindow(HeadlessWindow&& p_window) = delete;

		~HeadlessWindow() override = default;

		/**
		 * Get the framebuffer size.
		 *
		 * \return The framebuffer size.
		 */
		[[nodiscard]] glm::uvec2 FramebufferSize() const override;

		/**
		 * Poll events of the window.
		 *
		 * \param p_eventQueue The event queue to add events to.
		 */
		void PollEvents(Bigfoot::EventQueue& p_eventQueue) const override;

#if defined BIGFOOT_WINDOWS
		/**
		 * Get the window instance.
		 *
		 * \return The window instance
		 */
		[[nodiscard]] HINSTANCE GetInstance() const override;

		/**
		 * Get the window handle.
		 *
		 * \return The window handle
		 */
		[[nodiscard]] HWND GetWindow() const override;
#elif defined BIGFOOT_LINUX
		/**
		 * Get the x11 window.
		 * 
		 * \return The x11 window
		 */
		[[nodiscard]] Window GetX11Window() const override;

		/**
		 * Get the x11 display.
		 * 
		 * \return The x11 display
		 */
		[[nodiscard]] Display* GetX11Display() const override;

		/**
		 * Get the wayland display.
		 * 
		 * \return The wayland display
		 */
		[[nodiscard]] wl_display* GetWaylandDisplay() const override;

		/**
		 * Get the wayland surface.
		 * 
		 * \return The wayland surface
		 */
		[[nodiscard]] wl_surface* GetWaylandSurface() const override;
#endif

#if defined BIGFOOT_VULKAN
		/**
		 * Get the require extensions.
		 *
		 * \return The required extensions.
		 */
		[[nodiscard]] Bigfoot::FastVector<const char*> RequiredExtensions() const override;
#endif

		HeadlessWindow& operator=(const HeadlessWindow& p_window) = delete;
		HeadlessWindow& operator=(HeadlessWindow&& p_window) = delete;
	};
}
