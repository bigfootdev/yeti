/*****************************************************************//**
 * \file   HeadlessWindow.cpp
 * 
 * \author Romain BOULLARD
 * \date   April 2023
 *********************************************************************/
#include <Window/HeadlessWindow.h>

#include <System/Event/EventQueue.h>
#include <System/Event/Window/EventWindow.h>

namespace Yeti
{
	glm::uvec2 HeadlessWindow::FramebufferSize() const
	{
		return { 800, 600 };
	}

	/****************************************************************************************/

	void HeadlessWindow::PollEvents(Bigfoot::EventQueue& p_eventQueue) const
	{
		p_eventQueue.Push(Bigfoot::MakeUnique<Bigfoot::EventWindowClose>());
	}

	/****************************************************************************************/

#if defined BIGFOOT_WINDOWS
	HINSTANCE HeadlessWindow::GetInstance() const
	{
		return nullptr;
	}

	/****************************************************************************************/

	HWND HeadlessWindow::GetWindow() const
	{
		return nullptr;
	}
#elif defined BIGFOOT_LINUX

	/****************************************************************************************/

	Window HeadlessWindow::GetX11Window() const
	{
		return 0;
	}

	/****************************************************************************************/

	Display* HeadlessWindow::GetX11Display() const
	{
		return nullptr;
	}

	/****************************************************************************************/

	wl_display* HeadlessWindow::GetWaylandDisplay() const
	{
		return nullptr;
	}

	/****************************************************************************************/

	wl_surface* HeadlessWindow::GetWaylandSurface() const
	{
		return nullptr;
	}
#endif

	/****************************************************************************************/

#if defined BIGFOOT_VULKAN
	Bigfoot::FastVector<const char*> HeadlessWindow::RequiredExtensions() const
	{
		return {};
	}
#endif
}
